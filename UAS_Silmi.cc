/*
Saya mengerjakan soal nomor 3
SILMI ARDHIANI
301230054
IF 1B
*/


#include <stdio.h>

struct Mahasiswa {
    char nama[50];
    double quiz, absen, uts, uas, tugas;
    double nilai;
    char hurufMutu;
};

int main() {
    int jumlahMahasiswa;
    printf("Masukkan jumlah mahasiswa: ");
    scanf("%d", &jumlahMahasiswa);

    struct Mahasiswa mahasiswa[jumlahMahasiswa];

    for (int i = 0; i < jumlahMahasiswa; ++i) {
        printf("\nMasukkan informasi mahasiswa ke-%d:\n", i + 1);
        printf("Nama: ");
        scanf("%s", mahasiswa[i].nama);
        printf("Nilai Quiz: ");
        scanf("%lf", &mahasiswa[i].quiz);
        printf("Nilai Absen: ");
        scanf("%lf", &mahasiswa[i].absen);
        printf("Nilai UTS: ");
        scanf("%lf", &mahasiswa[i].uts);
        printf("Nilai UAS: ");
        scanf("%lf", &mahasiswa[i].uas);
        printf("Nilai Tugas: ");
        scanf("%lf", &mahasiswa[i].tugas);

        mahasiswa[i].nilai = ((0.1 * mahasiswa[i].absen) + (0.2 * mahasiswa[i].tugas) +
                            (0.3 * mahasiswa[i].quiz) + (0.4 * mahasiswa[i].uts) +
                            (0.5 * mahasiswa[i].uas)) / 2 ;

        if (mahasiswa[i].nilai > 85 && mahasiswa[i].nilai <= 100)
            mahasiswa[i].hurufMutu = 'A';
        else if (mahasiswa[i].nilai > 70 && mahasiswa[i].nilai <= 85)
            mahasiswa[i].hurufMutu = 'B';
        else if (mahasiswa[i].nilai > 55 && mahasiswa[i].nilai <= 70)
            mahasiswa[i].hurufMutu = 'C';
        else if (mahasiswa[i].nilai > 40 && mahasiswa[i].nilai <= 55)
            mahasiswa[i].hurufMutu = 'D';
        else
            mahasiswa[i].hurufMutu = 'E';
    }

    printf("\nHasil perhitungan nilai:\n");
    for (int i = 0; i < jumlahMahasiswa; ++i) {
        printf("Mahasiswa %s - Nilai: %.2lf - Huruf Mutu: %c\n",
               mahasiswa[i].nama, mahasiswa[i].nilai, mahasiswa[i].hurufMutu);
    }

    return 0;
}
